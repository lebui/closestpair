package training.codequality.lebui2426;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public final class ClosestPair {	 

	Point point1;
	Point point2;
	
	private double globalMinDistance = Double.MAX_VALUE; 
	
    /**
     * Find closest distance pairs of points
     * @param inputPoints the array of input points
     * @return the closest distance pairs of points
     */
    public double findClosestDistancePairs(final Point[] inputPoints) {
        quickSort(inputPoints, 0, inputPoints.length - 1, Point.xComparator);
	    return doFindClosestDistancePairs(inputPoints, inputPoints.length);
	}
	
	private double doFindClosestDistancePairs(final Point[] inputPoints, final int numberOfPoints) {
        if (numberOfPoints <= 3) {
            return bruteForce(inputPoints);
        }
    
		final int divideX = numberOfPoints / 2;		
		final Point[] leftArray = Arrays.copyOfRange(inputPoints, 0, divideX);
		final Point[] rightArray = Arrays.copyOfRange(inputPoints, divideX, numberOfPoints);
		
		final double leftAreaMinDistance = doFindClosestDistancePairs(leftArray, divideX);
		final double rightAreaMinDistance = doFindClosestDistancePairs(rightArray, numberOfPoints - divideX);
		double minDistance = Math.min(leftAreaMinDistance, rightAreaMinDistance);

		final Point[] window = createWindow(inputPoints, divideX, minDistance);
		quickSort(window, 0, window.length - 1, Point.yComparator);
		minDistance = compareWindowPoints(minDistance, window);
		
		return minDistance;
	}

    private double compareWindowPoints(double minDistance, final Point[] window) {
		for (int i = 0; i < window.length - 1; i++) { 
			for (int j = i + 1; j < window.length; j++) {
				if (Point.yGap(window[i], window[j]) < minDistance) {
					double distance = Point.distance(window[i], window[j]);
					
					if (distance < minDistance) { 
						minDistance = distance;
						if (distance < this.globalMinDistance) { 
							this.globalMinDistance = distance;
							point1 = window[i];
							point2 = window[j];
						}
					}
				} 
				else {
					break;
				}	
			}
		}
        return minDistance;
    }

    /**
     * Find closest distance pairs of points using brute force algorithm, used when the number of points is less than 3.
     * @param inputPoints the array of input points
     * @return the closest distance pairs of points
     */
    private double bruteForce(final Point[] inputPoints) {
        if (inputPoints.length == 1) {
            return 0.0;
        }
        
        double minDistance = Double.MAX_VALUE;
        double distance;

        for (int i = 0; i < inputPoints.length - 1; i++) {
            for (int j = (i + 1); j < inputPoints.length; j++) {
                distance = Point.distance(inputPoints[i], inputPoints[j]);
                
                if (distance < minDistance) {
                    minDistance = distance;
                    if (distance < this.globalMinDistance) {
                        this.globalMinDistance = distance;
                        this.point1 = inputPoints[i];
                        this.point2 = inputPoints[j];
                    }
                }
            }
        }
        return minDistance;

    }
    
    private Point[] createWindow(final Point[] divideArray, final int divideX, final double minDistance) {
        final List<Point> firstWindowList = new LinkedList<>();
        
        for (int i = 0; i <= divideX; i++) {
            final double xGap = Point.xGap(divideArray[i], divideArray[divideX]);
            if (xGap < minDistance) {
                firstWindowList.add(divideArray[i]);
            }
        }
        
        return firstWindowList.toArray(new Point[firstWindowList.size()]);
    }
    
    private void quickSort(final Point[] points, final int firstIndex, final int lastIndex, final Comparator<Point> comparator) {
        if (firstIndex < lastIndex) {
            final int pivot = partition(points, firstIndex, lastIndex, comparator);
            quickSort(points, firstIndex, pivot - 1, comparator);
            quickSort(points, pivot + 1, lastIndex, comparator);
        }
    }

    private int partition(final Point[] points, final int firstIndex, final int lastIndex, final Comparator<Point> comparator) {
        final int pivot = lastIndex;
        int firstGreaterThanPivot = firstIndex - 1;
        
        for (int i = firstIndex; i < lastIndex; i++) {
            if (comparator.compare(points[i], points[pivot]) <= 0) {
                firstGreaterThanPivot++;
                swap(points, firstGreaterThanPivot, i);
            }
        }
        
        firstGreaterThanPivot++;
        swap(points, firstGreaterThanPivot, pivot);
        
        return firstGreaterThanPivot;
    }

    private void swap(final Point[] points, final int firstIndex, final int secondIndex) {
        Point temp = points[firstIndex];
        points[firstIndex] = points[secondIndex];
        points[secondIndex] = temp;
    }

	public static void main(final String[] args) {
		final Point[] inputPoints = initializeInputPoints();
		System.out.println("Input data");
		System.out.println("Number of points: "+ inputPoints.length);
        Arrays.stream(inputPoints).forEach(point -> System.out.println(point));

        final ClosestPair closestPair = new ClosestPair();
		final double result = closestPair.findClosestDistancePairs(inputPoints);
		
		System.out.println("Output Data"); 
		System.out.println(closestPair.point1);
		System.out.println(closestPair.point2);
		System.out.println("Minimum Distance : " + result);

	}

    private static Point[] initializeInputPoints() {
        final Point[] inputPoints = new Point[] {
            new Point(2, 3),
            new Point(2, 16),
            new Point(3, 9),
            new Point(6, 3),
            new Point(7, 7),
            new Point(19, 4),
            new Point(10, 11),
            new Point(15, 2),
            new Point(15, 19),
            new Point(16, 11),
            new Point(17, 13),
            new Point(9, 12)
        };

        return inputPoints;
    }
}
