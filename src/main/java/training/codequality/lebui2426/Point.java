package training.codequality.lebui2426;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Class that represent a point in 2D coordinate system
 */
public class Point {

    private double x;

    private double y;

    public static final Comparator<Point> xComparator = Comparator.comparingDouble(point -> point.getX());

    public static final Comparator<Point> yComparator = Comparator.comparingDouble(point -> point.getY());

    /**
     * Regular expression pattern to match "(x,y)" format, where the types of x and y are double 
     */
    private static final Pattern PARSE_PATTERN = Pattern.compile("\\(([-+]?\\d+(\\.\\d+)?),([-+]?\\d+(\\.\\d+)?)\\)");

    public Point() {
        this.setX(0);
        this.setY(0);
    }

    public Point(final double x, final double y) { // Save x, y coordinates
        this.setX(x);
        this.setY(y);
    }

    /**
     * Parse string to Point object in (x,y) format
     * @param pointString the string representation of the point, must be in (x,y) format
     * @return Point object corresponding to the input string
     */
    public static Point parse(final String pointString) {
        Matcher matcher = PARSE_PATTERN.matcher(pointString);
        matcher.matches();

        double x = Double.parseDouble(matcher.group(1));
        double y = Double.parseDouble(matcher.group(3));
        return new Point(x, y);
    }

    public static double distance(final Point point1, final Point point2) {
        double xGap = point1.getX() - point2.getX();
        double yGap = point1.getY() - point2.getY();
        return Math.sqrt(Math.pow(xGap, 2) + Math.pow(yGap, 2));
    }

    public static double xGap(final Point point1, final Point point2) {
        return Math.abs(point1.getX() - point2.getX());
    }

    public static double yGap(final Point point1, final Point point2) {
        return Math.abs(point1.getY() - point2.getY());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(getX());
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getY());
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Point other = (Point) obj;
        if (Double.doubleToLongBits(getX()) != Double.doubleToLongBits(other.getX()))
            return false;
        if (Double.doubleToLongBits(getY()) != Double.doubleToLongBits(other.getY()))
            return false;
        return true;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", this.getX(), this.getY());
    }
}